package com.example.TransformAPI

import org.apache.spark.sql.SparkSession
import org.springframework.web.bind.annotation.RestController
@RestController
class Input {
  var pipelineID: Option[Integer] = None
  var transformID: Option[Integer] = None
  var outputDfName: String = "" //"studteach"
  var inputDfList: String = "" //"student,teacher" //Array[String] = _
  var transformQuery: String = ""

  def TransformQuery() {
    val spark = SparkSession.builder
      .master("local[*]")
      .appName("spark_task")
      .getOrCreate()
    val url = "jdbc:mysql://localhost:3306/dataeaze"

    val dataframeState = spark.read
      .format("jdbc")
      .option("driver", "com.mysql.cj.jdbc.Driver")
      .option("url", url)
      .option("dbtable", "dataframeState")
      .option("user", "root")
      .option("password", "dataeaze")
      .load()

    val obj = new Input()

    val datapipelindId = obj.pipelineID
    val transformId = obj.transformID
    val df_tables = obj.inputDfList //"student,teacher"
    val inputDfList: Array[String] = df_tables.split(",")
    val transformQuery = obj.transformQuery

    val newTableName = obj.outputDfName

    for (i <- inputDfList) {
      dataframeState.createOrReplaceTempView("dataframeState")
      val std1 = spark.sql(
        "select df_content from dataframeState where dataframeName='" + i + "'"
      )

      val dfdata =
        std1.select("df_content").rdd.map(r => r(0)).collect.mkString("")
      import spark.implicits._
      val jsonDataset = Seq(dfdata).toDS()
      val df = spark.read.json(jsonDataset)

      df.createOrReplaceTempView(" " + i + " ")
    }

    val record = spark.sql(transformQuery)
    record.createOrReplaceTempView(
      "" + newTableName + ""
    ) //creating a new dataframe and then storing it in mysql table
    var schemas = spark.sql("desc " + newTableName)
    schemas = schemas.withColumnRenamed("col_name", "fieldName")
    schemas = schemas.withColumnRenamed("data_type", "DataType")
    schemas = schemas.drop("comment")

    val colname_dataType = schemas.toJSON

    val df_schema = colname_dataType
      .select("value")
      .rdd
      .map(r => r(0))
      .collect
      .mkString(",")

    val df_content_Json = record.toJSON
    //record.show()
    val df_content =
      df_content_Json.select("value").rdd.map(r => r(0)).collect.mkString(",")
    //print(defg)

    val jsonArr = record.collect().toList
    print(jsonArr)

    val newdf = spark.sql(
      "insert into dataframeState values(" + datapipelindId + "," + transformId + ",'" + newTableName + "'," + "'[" + df_schema + "]'" + ",'[" + df_content + "]' )"
    )

    newdf.write
      .format("jdbc")
      .option("url", url)
      .mode("append")
      .option("dbtable", "dataframeState")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("user", "root")
      .option("password", "dataeaze")
      .save()

    //    newdf.printSchema()

  }
}
/*{
  "pipeline_id" : "",
  "transform_id" : "",
  "output_df_name" : "",
  "input_df_list" : "<csl of df names>",
  "transform_query" : "<transform query>"
}*/
//  object pract {
//    def main(args: Array[String]): Unit = {}
//    val input = new com.example.TransformAPI.Input;
//    input.TransformQuery()
//  }
